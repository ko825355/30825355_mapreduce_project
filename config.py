# Specify folder path
FILE_LOC = "data/AComp_Passenger_data_no_error.csv"              # Specify the path of the dataset

# Define target column index
TARGET_COL_INDEX = 0

# Define column value pattern
VALUE_PATTERN = '^[a-zA-Z0-9_]+$'

# To perform preprocessing
DO_PREPROCESS = True

# To perform missing value and duplicate row removal
MISSING_VAL_REMOVE = True
ROW_DUPLICATE_REMOVE = True

# Title and labels for bar graph
GRAPH_TITLE = "Number of Flights by Passenger ID"
GRAPH_X_AXIS_TITLE = "Passenger ID"
GRAPH_Y_AXIS_TITLE = "Number of Flights"
