import pandas as pd
from config import FILE_LOC, DO_PREPROCESS, TARGET_COL_INDEX, GRAPH_TITLE, GRAPH_X_AXIS_TITLE, GRAPH_Y_AXIS_TITLE
from data_preprocess import preprocess_data
from mapred_functions import mapper_func, reducer_func, shuffle_func
from plot_generator import plot_gen_func
import multiprocessing as mp

# Read the data files
data_df = pd.read_csv(FILE_LOC, header=None)

# Data preprocessing
if(DO_PREPROCESS):
    data_df = preprocess_data(data_df)

# Create list of passenger IDs
data_arr = list(data_df[TARGET_COL_INDEX])

# Multiprocessing
if __name__ == '__main__':
    with mp.Pool(processes=mp.cpu_count()) as pool:
        map_out = pool.map(mapper_func, data_arr, chunksize=int(len(data_arr)/mp.cpu_count()))

        shuffle_out = shuffle_func(map_out)

        reduce_out = pool.map(reducer_func, shuffle_out.items(), chunksize=int(len(shuffle_out.keys())/mp.cpu_count()))
        reduce_out.sort(key=lambda data: data[1], reverse=True)

        print("##" * 20)
        print(f"Count of flights taken by passengers:\n {pd.DataFrame (reduce_out, columns=['passenger_ID', 'count'])}")
        print("##" * 20)

        x_val, y_val = zip(*reduce_out)
        plot_gen_func(x=x_val,
                      y=y_val,
                      title=GRAPH_TITLE,
                      x_label=GRAPH_X_AXIS_TITLE,
                      y_label=GRAPH_Y_AXIS_TITLE)
