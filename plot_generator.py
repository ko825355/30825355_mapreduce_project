import matplotlib.pyplot as plt
import numpy as np

'''
    Generate a bar plot with the x and y values passed as parameters
'''
def plot_gen_func(x, y, title=None, x_label=None, y_label=None):
    x_pos = np.arange(len(x))

    plt.bar(x_pos, y, align='center')
    plt.xticks(x_pos, x, rotation=45, ha="right")
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    plt.title(title)
    plt.show()
    plt.tight_layout()
