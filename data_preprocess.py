from config import TARGET_COL_INDEX, MISSING_VAL_REMOVE, ROW_DUPLICATE_REMOVE

def preprocess_data(df):
    df_copy = df.copy()

    if(MISSING_VAL_REMOVE):
        # Filter out missing values
        df_copy = df_copy[df_copy[TARGET_COL_INDEX].notnull()]

    if(ROW_DUPLICATE_REMOVE):
        # Remove duplicates
        df_copy = df_copy.drop_duplicates(keep='first', ignore_index=True)

    return df_copy
