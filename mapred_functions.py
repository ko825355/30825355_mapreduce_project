import re
from config import VALUE_PATTERN

'''
    Returns a tuple indexing the occurrence a particular key if the key matches the pattern specified by the user.
'''
def mapper_func(key_x):
    if re.match(VALUE_PATTERN, key_x):
        return (key_x, 1)
    else:
        print(f"Error: {key_x} is an invalid key. Pattern does not match.")
        pass


'''
    Combines the shuffler output and presents in readable format for user
'''
def reducer_func(obj):
    ky, val = obj
    return (ky, sum(val))


'''
    Shuffler will organize the output of the mapper function based on their keys
    The output of the Shuffler would then be passed to the Reducer
'''
def shuffle_func(mapped_val):
    return_data = {}

    mapped_val = list(filter(None, mapped_val))

    for key, val in mapped_val:
        if key not in return_data:
            return_data[key] = [val]
        else:
            return_data[key].append(val)
    return return_data
